#!/bin/bash
service mysql stop
apt-get purge php5 apache2 -y
apt-get remove mysql-server -y
apt-get autoremove -y
rm -f /etc/apache2/sites-available/appuitest.cgparivar.com.conf
rm -rf /var/www/html/*

apt-get update -y
export DEBIAN_FRONTEND=noninteractive
sudo -E apt-get -q -y install mysql-server
service mysql start
apt-get install apache2 php5  -y
#mysqladmin -u root password cgparivar 
scp vagrant@192.168.0.38:/home/vagrant/appui* /var/www/html/
cd /var/www/html/
rm -f index.html
tar -xzf appui.test.tgz
#mysql -e -uroot -p'cgparivar' "create database TestSaralAppApi;"
#mysql -uroot -p'cgparivar' TestSaralAppApi < appuitest.sql
mysql -uroot -p'password' -e "create database TestSaralAppApi;"
mysql -uroot -p'password' TestSaralAppApi < appuitest.sql
chown -R www-data. /var/www/html/*
#sed -i 's/cgparivar123/cgparivar/g' database.php
sed -i 's/@#appuitest@#!123/password/g' database.php
touch /etc/apache2/sites-available/appuitest.cgparivar.com.conf

echo "<VirtualHost *:80>" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "ServerName 192.168.0.41" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  ## Vhost docroot" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  DocumentRoot "/var/www/html"" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  ## Directories, there should at least be a declaration for /opt/cgpits" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  <Directory "/var/www/html">" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "    Options Indexes FollowSymLinks MultiViews" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "    AllowOverride All" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "    Require all granted" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  </Directory>" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  <Files xmlrpc.php>" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "   Order Allow,Deny" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  Allow from localhost" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "   Deny from all" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "   </Files>" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  ## Logging" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  ErrorLog "/var/log/apache2/appuitest.cgparivar.com_error.log"" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  ServerSignature Off" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "  CustomLog "/var/log/apache2/appuitest.cgparivar.com_access.log" combined" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf
echo "</VirtualHost>" >> /etc/apache2/sites-available/appuitest.cgparivar.com.conf

a2ensite appuitest.cgparivar.com.conf
service apache2 restart



 


