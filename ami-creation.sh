#!/bin/bash

echo "Enter AWS Region"
read region
export AWS_DEFAULT_REGION=$region
echo "Enter AWS Access Key"
read access_key
export AWS_ACCESS_KEY_ID=$access_key
echo "Enter AWS Secret Key"
read secret_key
export AWS_SECRET_ACCESS_KEY=$secret_key

DATE=$(date +%Y-%m-%d_%H-%M) 
AMI_NAME="AMI Backup - $DATE"
AMI_DESCRIPTION="AMI Backup - $DATE"
REGION_NAME=$1

/usr/bin/aws ec2 describe-instances | jq -r '.Reservations[] | .Instances[]| .InstanceId' 

printf "Requesting AMI for instance $1...\n"
echo "Please enter instance id"
read instanceid
aws ec2 create-image --instance-id $instanceid --name "$AMI_NAME" --description "$AMI_DESCRIPTION" --no-reboot

if [ $? -eq 0 ]; then
	printf "AMI request complete!\n"
fi
