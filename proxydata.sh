#!/bin/bash
echo "Enter path where log file is present"
read filepath
echo "Enter log file name"
read filename
echo "Enter date ex: (08/Jun)"
read log_date
cd $filepath
grep $log_date $filename | cut -d[ -f3 | cut -d] -f1 | awk -F: '{print $2 ":00"}' | sort -n | uniq -c
