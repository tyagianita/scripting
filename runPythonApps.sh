#!/bin/bash
echo -e "Restarting old running instancees of apps\n"
echo "Restarting admin_python\n"
echo -e "Stopping process pid `ps -ef | grep "python /home/admin_python/manage.py runserver" |grep -v grep | awk {'print $2'}`\n"
ps -ef | grep "python /home/admin_python/admin_python/manage.py runserver" |grep -v grep | awk {'print $2'} | xargs kill -9
cd /home/admin_python/admin_python/
source /home/admin_python/admin_python/env/bin/activate
echo -e "\nStarting new instance of app"
nohup python /home/admin_python/admin_python/manage.py runserver 0.0.0.0:8000 &


echo -e "Restarting doctorapi\n"
echo -e "Stopping process pid `ps -ef | grep "python /home/doctorapi/doctorapi/manage.py runserver" |grep -v grep | awk {'print $2'}`\n"
ps -ef | grep "python /home/doctorapi/doctorapi/manage.py runserver" |grep -v grep | awk {'print $2'} | xargs kill -9
cd /home/doctorapi/doctorapi
source /home/doctorapi/doctorapi/env/bin/activate
echo -e "\nStarting new instance of app"
nohup python /home/doctorapi/doctorapi/manage.py runserver 0.0.0.0:8400 &


echo -e "Restarting doctor_python\n"
echo -e "Stopping process pid `ps -ef | grep "python /home/doctor_python/doctor_python/manage.py runserver" |grep -v grep | awk {'print $2'}`\n"
ps -ef | grep "python /home/doctor_python/doctor_python/manage.py runserver" |grep -v grep | awk {'print $2'} | xargs kill -9
cd /home/doctor_python/doctor_python/
source /home/doctor_python/doctor_python/env/bin/activate
echo -e "\nStarting new instance of app"
nohup python /home/doctor_python/doctor_python/manage.py runserver 0.0.0.0:8200 &


echo -e "Restarting userapi\n"
echo -e "Stopping process pid `ps -ef | grep "python /home/userapi/manage.py runserver" |grep -v grep | awk {'print $2'}`\n"
ps -ef | grep "python /home/userapi/userapi/manage.py runserver" |grep -v grep | awk {'print $2'} | xargs kill -9
cd /home/userapi/userapi
source /home/userapi/userapi/env/bin/activate
echo -e "\nStarting new instance of app"
nohup python /home/userapi/userapi/manage.py runserver 0.0.0.0:8100 &


echo -e "Restarting docinstaapi\n"
echo -e "Stopping process pid `ps -ef | grep "/home/docinstaapi/docinstaapi/docapi/env/bin/python /home/docinstaapi/docinstaapi/docapi/manage.py runserver" |grep -v grep | awk {'print $2'}`\n"
ps -ef | grep "/home/docinstaapi/docinstaapi/docapi/env/bin/python /home/docinstaapi/docinstaapi/docapi/manage.py runserver" |grep -v grep | awk {'print $2'} | xargs kill -9
cd /home/docinstaapi/docinstaapi/docapi
source /home/docinstaapi/docinstaapi/docapi/env/bin/activate
echo -e "\nStarting new instance of app"
nohup /home/docinstaapi/docinstaapi/docapi/env/bin/python /home/docinstaapi/docinstaapi/docapi/manage.py runserver 0.0.0.0:8500 &

echo -e "Restarting businessapi\n"
echo -e "Stopping process pid `ps -ef | grep "python /home/businessapi/businessapi/manage.py runserver" |grep -v grep | awk {'print $2'}`\n"
ps -ef | grep "python /home/businessapi/businessapi/manage.py runserver" |grep -v grep | awk {'print $2'} | xargs kill -9
cd /home/businessapi/businessapi
source /home/businessapi/businessapi/env/bin/activate
echo -e "\nStarting new instance of app"
nohup python /home/businessapi/businessapi/manage.py runserver 0.0.0.0:9000 &

echo -e "Restarting business_python\n"
echo -e "Stopping process pid `ps -ef | grep -v grep | grep "/home/business_python/business_python/env/bin/python manage.py" | awk {'print $2'}`"
ps -ef | grep "/home/business_python/business_python/env/bin/python manage.py runserver" | grep -v grep | awk {'print $2'} | xargs kill -9
cd /home/business_python/business_python
source /home/business_python/business_python/env/bin/activate
echo -e "\nStarting new instance of app"
nohup python /home/business_python/business_python/manage.py runserver 0.0.0.0:9100 &

echo "Process Ids For Apps Are"

echo -e "\nProcess id for userapi is `ps -ef | grep -v grep | grep "python /home/userapi/userapi/manage.py runserver" | grep -v "env/bin" | awk {'print $2'}`"
echo -e "\nProcess id for admin_python is `ps -ef | grep -v grep | grep "python /home/admin_python/admin_python/manage.py runserver" | grep -v "env/bin" | awk {'print $2'}`"
echo -e "\nProcess id for doctorapi is `ps -ef | grep -v grep | grep "python /home/doctorapi/doctorapi/manage.py runserver" | grep -v "env/bin" | awk {'print $2'}`"
echo -e "\nProcess id for doctor_python is `ps -ef | grep -v grep | grep "python /home/doctor_python/doctor_python/manage.py runserver" | grep -v "env/bin" | awk {'print $2'}`"
echo -e "\nProcess id for docinstaapi is `ps -ef | grep -v grep | grep "/home/docinstaapi/docinstaapi/docapi/env/bin/python /home/docinstaapi/docinstaapi/docapi/manage.py runserver" | awk {'print $2'}`"
echo -e "\nProcess id for businessapi is `ps -ef | grep -v grep | grep "/home/businessapi/businessapi/env/bin/python /home/businessapi/businessapi/manage.py runserver" | awk {'print $2'}`"
echo -e "\nProcess id for business_python is `ps -ef | grep -v grep | grep "/home/business_python/business_python/env/bin/python manage.py" | awk {'print $2'}`"
