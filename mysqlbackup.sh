#!/bin/sh

cd /root/

Date=`date +%F`

Pre_Date=`date --date yesterday +"%F"`

Dump=dr_insta-$Date-$Date-backup.sql

Last_Dump=dr_insta-$Pre_Date-$Pre_Date-backup.sql

/usr/bin/mysqldump -uroot -p'pass' newdb > $Dump

/usr/local/bin/aws s3 cp $Dump s3://bmx-prod-backup/mysql-data/

/bin/rm -f $Last_Dump
