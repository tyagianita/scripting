

#!/bin/bash

echo "Starting with Blue Green Deployment"
BLUE_BASE_DIR=blue_green
BLUE_DIR=`date +'%d_%m_%Y_%H_%M_%S'`
GREEN_DIR=public_html2
function createBlueSetup() {
        echo "Creating blue setup"

        mkdir -pv ${BLUE_BASE_DIR}/${BLUE_DIR}
        tar -xzf /tmp/iziphub.tar.gz -C ${BLUE_BASE_DIR}/${BLUE_DIR}
}

function deploy_blue_green() {
        echo "Doing blue green switch"
        rm -fv ${GREEN_DIR}
        ln -sv ${BLUE_BASE_DIR}/${BLUE_DIR} ${GREEN_DIR}
}

function cleanup_blue_setup() {
        echo "Cleaning up blue space"
        cd ${BLUE_BASE_DIR}
        echo "Will clean up below directories"
        ls -1tr . | head -n -2
        rm -rf $(ls -1tr . | head -n -2)
}

createBlueSetup
deploy_blue_green
cleanup_blue_setup


