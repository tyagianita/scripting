#!/bin/sh

PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

if [ -z "$APP_HOME" ]
then
  APP_SH=$0
  case "$APP_SH" in
    /*)     APP_HOME=${APP_SH%/*/*} ;;
    ./*/*)  APP_HOME=${APP_SH%/*/*} ;;
    ./*)    APP_HOME=.. ;;
    */*/*)  APP_HOME=./${APP_SH%/*/*} ;;
    */*)    APP_HOME=. ;;
    *)      APP_HOME=.. ;;
  esac
fi

cd "$APP_HOME"
APP_HOME=$PWD
cd "$APP_HOME/bin"

APP_HOME_DIR=""

if [ -z "$APP_HOME" ]; then
    APP_HOME_DIR=$APP_HOME
else
    APP_HOME_DIR=$APP_HOME
fi


# Raspberry Pi check (Java VM does not run with -server argument on ARMv6)
if [ `uname -m` != "armv6l" ]; then
  JAVA_OPTS="$JAVA_OPTS -server "
fi

JAVA_OPTS="$JAVA_OPTS -Dapp.home.dir=$APP_HOME_DIR"

export JAVA_OPTS

# Set JavaHome if it exists
if [ -f "${JAVA_HOME}/bin/java" ]; then
   JAVA=${JAVA_HOME}/bin/java
else
   JAVA=java
fi
export JAVA

WWW_PATH=$APP_HOME
APP_PID=$APP_HOME/bin/app.pid

if [ -f "$APP_PID" ]; then
    echo "removing old pid file $APP_PID"
    rm "$APP_PID"
fi

# DEBUG OPTS, SIMPLY USE 'startup.sh debug'
DEBUG_OPTS=""
ARGS='';
for var in "$@"; do
    if [ "$var" = "debug" ]; then
        DEBUG_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1044"
    else
        ARGS="$ARGS $var"
    fi
done

# APP memory options, default to 512 of heap.

if [ -z "$APP_OPTS_MEMORY" ] ; then
    APP_OPTS_MEMORY="-Xms512m -Xmx512m"
fi

if [ -z "$JAVA_OPTS_SCRIPT" ] ; then
    JAVA_OPTS_SCRIPT="-Djna.nosys=true -XX:+HeapDumpOnOutOfMemoryError -XX:MaxDirectMemorySize=512g -Djava.awt.headless=true -Dfile.encoding=UTF8 -Drhino.opt.level=9"
fi

echo $$ > $APP_PID

CLASSPATH=.:$APP_HOME/etc
CLASSPATH=$CLASSPATH:$APP_HOME/lib/*

exec "$JAVA" $JAVA_OPTS $APP_OPTS_MEMORY $JAVA_OPTS_SCRIPT $DEBUG_OPTS \
    -Dcarot.platform.path="$WWW_PATH" \
    -cp $CLASSPATH \
    $ARGS com.minda.iconnect.platform.runner.Runner --spring com.minda.iconnect.carot.server.CarotServerConfig
