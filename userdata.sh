#!/bin/bash

#clone the code of doctor_python
su -  admin_python <<'EOF'
cp -r admin_python/ admin_python.bck
mkdir /home/admin_python/old  
cd /home/admin_python/old
git clone gitolite3:admin_python.git
cd /home/admin_python
rsync -r old/admin_python/ admin_python/
rm -rf old
EOF

#clone the code of doctor_python
su -  business_python <<'EOF'
cp -r business_python/ business_python.bck
mkdir /home/business_python/old  
cd /home/business_python/old
git clone gitolite3:business_python.git
cd /home/business_python
rsync -r old/business_python/ business_python/
rm -rf old
EOF

#clone the code of doctor_python
su -  businessapi <<'EOF'
cp -r businessapi businessapi.bck
mkdir /home/businessapi/old  
cd /home/businessapi/old
git clone gitolite3:businessapi.git
cd /home/businessapi
rsync -r old/businessapi/ businessapi/
rm -rf old
EOF

#clone the code of doctor_python
su -  doctorapi <<'EOF'
cp -r doctorapi/ doctorapi.bck
mkdir /home/doctorapi/old  
cd /home/doctorapi/old
git clone gitolite3:doctorapi.git
cd /home/doctorapi
rsync -r old/doctorapi/ doctorapi/
rm -rf old
EOF

#clone the code of doctor_python
su -  doctorinsta-front <<'EOF'
cp -r doctorinsta-front/ doctorinsta-front.bck
mkdir /home/doctorinsta-front/old  
cd /home/doctorinsta-front/old
git clone gitolite3:doctorinsta-front.git
cd /home/doctorinsta-front
rsync -r old/doctorinsta-front/ doctorinsta-front/
rm -rf old
EOF

#clone the code of doctor_python
su -  doctor_python <<'EOF'
cp -r doctor_python/ doctor_python.bck
mkdir /home/doctor_python/old  
cd /home/doctor_python/old
git clone gitolite3:doctor_python.git
cd /home/doctor_python
rsync -r old/doctor_python/ doctor_python/
rm -rf old
EOF

#clone the code of webapp_angular
su -  webapp_angular <<'EOF'
cp -r webapp_angular/ webapp_angular.bck
mkdir /home/webapp_angular/old  
cd /home/webapp_angular/old
git clone gitolite3:webapp_angular.git
cd /home/webapp_angular
rsync -r old/webapp_angular/ webapp_angular/
rm -rf old
EOF

#clone the code of doctor_python
su -   userapi <<'EOF'
cp -r  userapi/  userapi.bck
mkdir /home/ userapi/old  
cd /home/ userapi/old
git clone gitolite3: userapi.git
cd /home/ userapi
rsync -r old/userapi/  userapi/
rm -rf old
EOF
